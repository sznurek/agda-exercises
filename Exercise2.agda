module Exercise2 where

 -- Chciałem, żeby dowód był 'self-contained', więc nie korzystałem
 -- z biblioteki standardowej. Minus jest taki, że musiałem napisać
 -- małą biblioteczkę ręcznie.

data ℕ : Set where
  zero : ℕ
  succ : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}
{-# BUILTIN ZERO zero #-}
{-# BUILTIN SUC succ #-}

data _×_ (A B : Set) : Set where
  pair : A → B → A × B

infix 5 _≡_

data _≡_ {A : Set} (a : A) : A → Set where
  refl : a ≡ a

subst : ∀{A : Set} {x y : A} → (P : A → Set) → x ≡ y → P x → P y
subst _ refl x = x

data Maybe (A : Set) : Set where
  Nothing : Maybe A
  Just : A → Maybe A

infixl 15 _>>=_

_>>=_ : {A B : Set} → Maybe A → (A → Maybe B) → Maybe B
Nothing >>= f = Nothing
Just x >>= f = f x

return : {A : Set} → A → Maybe A
return = Just

bind1 : {A B : Set} → (A → Maybe B) → Maybe A → Maybe B
bind1 f x = x >>= f

bind2 : {A B C : Set} → (A → B → Maybe C) → Maybe A → Maybe B → Maybe C
bind2 f x y = x >>= λ ux → y >>= λ uy → f ux uy

bind3 : {A B C D : Set} → (A → B → C → Maybe D) → Maybe A → Maybe B → Maybe C → Maybe D
bind3 f x y z = x >>= λ ux → y >>= λ uy → z >>= λ uz → f ux uy uz


lift1 : {A B : Set} → (A → B) → Maybe A → Maybe B
lift1 f x = bind1 (λ x → return (f x)) x

lift2 : {A B C : Set} → (A → B → C) → Maybe A → Maybe B → Maybe C
lift2 f x y = bind2 (λ x y → return (f x y)) x y

-- Uff, już po części bibliotecznej, dużo tego nie było. Teraz przepisywanie
-- definicji!

infixr 20 _⇒_
infixl 25 _⨂_

data Tp : Set where
  nat : Tp
  _⨂_ : Tp → Tp → Tp
  _⇒_ : Tp → Tp → Tp

data Expr : Set where
  N : ℕ → Expr
  if0 : Expr → Expr → Expr → Expr
  V : Expr
  lam : Tp → Expr → Expr
  _•_ : Expr → Expr → Expr
  [_,_] : Expr → Expr → Expr
  fst : Expr → Expr
  snd : Expr → Expr

infix 10 _⊢_∷_

-- Polecenie 1.

data _⊢_∷_ : Tp → Expr → Tp → Set where
  T-Nat : ∀{T} → { n : ℕ } → T ⊢ N n ∷ nat
  T-Var : ∀{T} → T ⊢ V ∷ T
  T-if0 : ∀{T T₀ b e1 e2} → T ⊢ b ∷ nat → 
           T ⊢ e1 ∷ T₀ → T ⊢ e2 ∷ T₀ → T ⊢ if0 b e1 e2 ∷ T₀
  T-lam : ∀{T A B e} → A ⊢ e ∷ B → T ⊢ lam A e ∷ A ⇒ B
  T-app : ∀{T A B e1 e2} → T ⊢ e1 ∷ A ⇒ B → T ⊢ e2 ∷ A →
           T ⊢ e1 • e2 ∷ B
  T-pair : ∀{T A B e1 e2} → T ⊢ e1 ∷ A → T ⊢ e2 ∷ B →
            T ⊢ [ e1 , e2 ] ∷ A ⨂ B
  T-fst : ∀{T A B e} → T ⊢ e ∷ A ⨂ B → T ⊢ fst e ∷ A
  T-snd : ∀{T A B e} → T ⊢ e ∷ A ⨂ B → T ⊢ snd e ∷ B

-- Polecenie 2

⟦_⟧ : Tp → Set
⟦ nat ⟧ = ℕ
⟦ x ⨂ x₁ ⟧ = ⟦ x ⟧ × ⟦ x₁ ⟧
⟦ x ⇒ x₁ ⟧ = ⟦ x ⟧ → ⟦ x₁ ⟧

eval : {T S : Tp} → (e : Expr) → T ⊢ e ∷ S → ⟦ T ⟧ → ⟦ S ⟧
eval (N x) T-Nat x₁ = x
eval (if0 e e₁ e₂) (T-if0 t t₁ t₂) x with eval e t x 
... | zero = eval e₁ t₁ x
... | succ _ = eval e₂ t₂ x
eval V T-Var x = x
eval (lam x e) (T-lam t) x₁ = eval e t
eval (f • x) (T-app t s) arg = eval f t arg (eval x s arg)
eval [ e₁ , e₂ ] (T-pair t₁ t₂) x = pair (eval e₁ t₁ x) (eval e₂ t₂ x)
eval (fst e) (T-fst t) x with eval e t x
... | pair a b = a
eval (snd e) (T-snd t) x with eval e t x 
... | pair a b = b

-- Polecenie 3.

data WellTyped (T : Tp) (e : Expr) : Set where
  hasType : (S : Tp) → T ⊢ e ∷ S → WellTyped T e

-- Tutaj potrzebowałem trochę brzydoty, żeby dostać rozstrzygalną
-- równość na typach. Dzięki niej nie potrzebowałem funkcji check-type.
--
-- Czemu tak? Wydaje mi się, że tak jest bardziej elegancko, bo podczas
-- inferencji nie chcę się upewniać, że jakiś term ma dany typ - ja ten
-- typ inferuję, a potem tylko sprawdzam, czy ma odpowiednią strukturę.

pairEqCong : ∀{a b c d : Tp} → a ≡ c → b ≡ d → a ⨂ b ≡ c ⨂ d
pairEqCong refl refl = refl

arrowEqCong : ∀{a b c d : Tp} → a ≡ c → b ≡ d → (a ⇒ b) ≡ (c ⇒ d)
arrowEqCong refl refl = refl

tpDecEq : ∀(T S : Tp) → Maybe (T ≡ S)
tpDecEq nat nat = Just refl
tpDecEq nat (y ⨂ y₁) = Nothing
tpDecEq nat (y ⇒ y₁) = Nothing
tpDecEq (x ⨂ x₁) nat = Nothing
tpDecEq (x ⨂ x₁) (y ⇒ y₁) = Nothing
tpDecEq (x ⇒ x₁) nat = Nothing
tpDecEq (x ⇒ x₁) (y ⨂ y₁) = Nothing
tpDecEq (x ⨂ x₁) (y ⨂ y₁) = lift2 pairEqCong (tpDecEq x y) (tpDecEq x₁ y₁)
tpDecEq (x ⇒ x₁) (y ⇒ y₁)  = lift2 arrowEqCong (tpDecEq x y) (tpDecEq x₁ y₁)

-- Żeby nie robić z funkcji infer-type kolosa, przypadki indukcyjne
-- rozbiłem na funkcje combine*. To w nich korzystam z tego, że mam
-- "półrozstrzygalną" równość na typach, co pozwala otypować m.in. ifa.

combineIf : {T : Tp} {n e1 e2 : Expr} → WellTyped T n
                  → WellTyped T e1 → WellTyped T e2
                  → Maybe (WellTyped T (if0 n e1 e2))
combineIf {T} {n} {e1} {e2} (hasType M prf) (hasType S₁ prf₁) (hasType S₂ prf₂) = 
  tpDecEq M nat >>= 
  λ eqn → tpDecEq S₁ S₂ >>=
  λ eq  → return (hasType S₂ (T-if0 (subst (λ P → T ⊢ n ∷ P) eqn prf) (subst (λ S → T ⊢ e1 ∷ S) eq prf₁) prf₂))))

combineLam : {T x : Tp} {e : Expr} → WellTyped x e → WellTyped T (lam x e)
combineLam {_} {x} (hasType S prf) = hasType (x ⇒ S) (T-lam prf)

combineApp : {T : Tp} {e₁ e₂ : Expr} → WellTyped T e₁ → WellTyped T e₂ → Maybe (WellTyped T (e₁ • e₂))
combineApp (hasType nat _) (hasType _ _) = Nothing
combineApp (hasType (_ ⨂ _) _) (hasType _ _) = Nothing
combineApp {T} {_} {e₂} (hasType (A ⇒ B) x) (hasType S y) =
  tpDecEq S A >>= (λ eq → return (hasType B (T-app x (subst (λ R → T ⊢ e₂ ∷ R) eq y))))

combinePair : {T : Tp} {e₁ e₂ : Expr} → WellTyped T e₁ → WellTyped T e₂ → WellTyped T [ e₁ , e₂ ]
combinePair {T} {e₁} {e₂} (hasType A a) (hasType B b) = hasType (A ⨂ B) (T-pair a b)

combineFst : {T : Tp} {e : Expr} → WellTyped T e → Maybe (WellTyped T (fst e))
combineFst (hasType nat x) = Nothing
combineFst (hasType (_ ⇒ _) x) = Nothing
combineFst (hasType (A ⨂ B) x) = return (hasType A (T-fst x))

combineSnd : {T : Tp} {e : Expr} → WellTyped T e → Maybe (WellTyped T (snd e))
combineSnd (hasType nat x) = Nothing
combineSnd (hasType (_ ⇒ _) x) = Nothing
combineSnd (hasType (A ⨂ B) x) = return (hasType B (T-snd x))

-- No, po takiej dawce kodu w infer-type zostało niewiele do napisania!

mutual
  infer-type : ∀ T e → Maybe (WellTyped T e)
  infer-type t (N x)         = return (hasType nat T-Nat)
  infer-type t V             = return (hasType t T-Var)
  infer-type t (if0 e e₁ e₂) = bind3 combineIf   (infer-type t e) (infer-type t e₁) (infer-type t e₂)
  infer-type t (lam x e)     = lift1 combineLam  (infer-type x e)
  infer-type t (e • e₁)      = bind2 combineApp  (infer-type t e) (infer-type t e₁)
  infer-type t [ e , e₁ ]    = lift2 combinePair (infer-type t e) (infer-type t e₁)
  infer-type t (fst e)       = bind1 combineFst  (infer-type t e)
  infer-type t (snd e)       = bind1 combineSnd  (infer-type t e)

-- Jeżeli wolelibyście jednak zobaczyć podejście z check-type to dajcie znać,
-- doślę co trzeba :) Ba, nawet definicja check-type będzie inna niż:
--
-- check-type _ _ = Nothing

-- Łukasz Dąbek.
