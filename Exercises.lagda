\documentclass{scrartcl}

% current annoyance: this will be fixed
% by the next update of agda.fmt
\def\textmu{}

%include agda.fmt

%format not = "not"

\usepackage[T1]{fontenc}
% \usepackage[utf8]{inputenc} 
\usepackage[polish]{babel} 
\usepackage{listings}

\newtheorem{zadanie}{Zadanie}

\author{Wojciech Jedynak \and Piotr Polesiuk}
\title{Ćwiczenia z Agdy -- część pierwsza}

\begin{document}
\lstset{language=Haskell}

\maketitle

\section{Uwaga}

Niniejszy dokument stanowi pierwszą część zadań z Agdy. Rozwiązując
wszystkie 9 zadań można uzyskać zaliczenie jednej listy seminaryjnej.
Wkrótce pojawi się lista druga (za drugi punkt), która będzie składać
się z jednego dużego (fascynującego!) zadania.

Lista zadań jest dostępna w dwu wariantach .pdf i .lagda. Wersję
.lagda można otworzyć w edytorze tekstu i uzupełniać brakujące
fragmenty bez przepisywania wszystkiego. Rozwiązania części pierwszej
przyjmujemy do \textbf{20 stycznia 2014 roku}. Rozwiązania w postaci
uzupełnionego pliku .lagda należy wysyłać e-mailem na adres
\texttt{wjedynak@@gmail.com} lub \texttt{piotr.polesiuk@@gmail.com}. Zachęcamy także do
zadawania pytań! 

\begin{code}
module Exercises where

\end{code}

\section{Podstawy Izomorfizmu Curry'ego-Howarda}

Fałsz zdefiniowaliśmy w Agdzie jako typ pusty:

\begin{code}

data ⊥ : Set where

⊥-elim : {A : Set} → ⊥ → A
⊥-elim ()

\end{code}

Możemy teraz wyrazić negację w standardowy sposób: jako funkcję w zbiór pusty.

\begin{code}

¬_ : Set → Set
¬ A = A → ⊥

\end{code}

\begin{zadanie}
Udowodnij, że p ⇒ ¬¬p, czyli dokończ poniższą definicję:

\begin{code}
pnnp : {A : Set} → A → ¬ ¬ A
pnnp = λ a p → p a
\end{code}

Czy potrafisz udowodnić implikację w drugą stronę?

Odpowiedź: Nie bardzo, ale później można pokazać pewną ciekawą implikację,
ale to już po zdefiniowaniu alternatywy.

\end{zadanie}

\begin{zadanie}

Zdefiniuj koniunkcję jako polimorficzny typ i udowodnij reguły eliminacji oraz prawo 
przemienności tj. zdefiniuj typ A ∧ B oraz funkcje fst, snd i swap:

\begin{code}

data _∧_ (A B : Set) : Set where
  and : A → B → A ∧ B

fst : {A B : Set} → A ∧ B → A
fst (and x x₁) = x

snd : {A B : Set} → A ∧ B → B
snd (and x x₁) = x₁

swap : {A B : Set} → A ∧ B → B ∧ A
swap (and x x₁) = and x₁ x

\end{code}

\end{zadanie}

\begin{zadanie}

Korzystając z koniunkcji z poprzedniego zadania i alternatywy z
wykładu, sformułuj i spróbuj udowodnić prawa De~Morgana znane z logiki
klasycznej. Które z nich zachodzą w logice kostruktywnej?

\end{zadanie}

\begin{code}
data _∨_ (A B : Set) : Set where
  inl : A → A ∨ B
  inr : B → A ∨ B

demorgan₁ : {A B : Set} → ¬ (A ∨ B) → (¬ A ∧ ¬ B)
demorgan₁ f = and (λ a → f (inl a)) (λ b → f (inr b))

{-
demorgan₂ : {A B : Set} → ¬ (A ∧ B) → (¬ A ∨ ¬ B)
demorgan₂ f = {!!}
-}

demorgan₃ : {A B : Set} → (¬ A ∧ ¬ B) → ¬ (A ∨ B)
demorgan₃ (and x x₁) (inl x₂) = x x₂
demorgan₃ (and x x₁) (inr x₂) = x₁ x₂

demorgan₄ : {A B : Set} → (¬ A ∨ ¬ B) → ¬ (A ∧ B)
demorgan₄ (inl x) (and x₁ x₂) = x x₁
demorgan₄ (inr x) (and x₁ x₂) = x x₂

\end{code}

\section{Liczby naturalne}

Na wykładzie zdefiniowaliśmy liczby naturalne z dodawaniem następująco:

\begin{code}

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}
{-# BUILTIN ZERO zero #-}
{-# BUILTIN SUC suc #-}

infix 6 _+_ 

_+_ : ℕ → ℕ → ℕ
zero  + m = m
suc n + m = suc (n + m)

\end{code}

\begin{zadanie}
Przypomnijmy definicję równości:

\begin{code}
infix 5 _≡_

data _≡_ {A : Set}(a : A) : A → Set where
  refl : a ≡ a

\end{code}

Pamiętając, że wg Izomorfizmu Curry'ego-Howarda indukcja to rekursja,
udowodnij następujące własności dodawania:

\begin{code}

subst : {A : Set} → (P : A → Set) → (a₁ a₂ : A) → (a₁ ≡ a₂) → P a₁ → P a₂
subst P .a₂ a₂ refl p = p

cong : {A B : Set} → (f : A → B) → (x y : A) → x ≡ y → f x ≡ f y
cong f .y y refl = refl

symm : {A : Set} → {x y : A} → x ≡ y → y ≡ x
symm refl = refl

plus-right-zero : (n : ℕ) → n + 0 ≡ n
plus-right-zero zero = refl
plus-right-zero (suc n) = cong suc (n + 0) n (plus-right-zero n)

plus-suc-n-m : (n m : ℕ) → suc (n + m) ≡ n + suc m
plus-suc-n-m zero m = refl
plus-suc-n-m (suc n) m = cong suc (suc (n + m)) (n + suc m) (plus-suc-n-m n m)
\end{code}

\end{zadanie}

\begin{zadanie}
Korzystając z poprzedniego zadania, udowodnij przemienność dodawania:

\begin{code}
plus-commutative : (n m : ℕ) → n + m ≡ m + n
plus-commutative zero m = symm (plus-right-zero m)
plus-commutative (suc n) m = subst (λ k → suc k ≡ m + suc n) (m + n) (n + m) (symm (plus-commutative n m)) ((plus-suc-n-m m n)) 
\end{code}

\end{zadanie}



\section{Wektory}

Przypomnijmy definicję wektorów:

\begin{code}

data Vec (A : Set) : ℕ → Set where
  []  : Vec A 0
  _∷_ : {n : ℕ} → (x : A) → (xs : Vec A n) → Vec A (suc n)

\end{code}

Zdefiniowaliśmy już m.in. konkatenację wektorów:

\begin{code}

_++_ : {A : Set} → {n m : ℕ} → Vec A n → Vec A m → Vec A (n + m)
[]       ++ v2 = v2
(x ∷ v1) ++ v2 = x ∷ (v1 ++ v2)

\end{code}

\begin{zadanie}

Zaprogramuj funkcję vmap, która jest wektorowym odpowiednikiem map dla list.
Jaka powinna być długość wynikowego wektora?

\end{zadanie}

\begin{code}

vmap : {A B : Set} {n : ℕ} → (f : A → B) → Vec A n → Vec B n
vmap f [] = []
vmap f (x ∷ vs) = f x ∷ vmap f vs

\end{code}

\begin{zadanie}

W Haskellu bardzo często używamy funkcji zip, która jest zdefiniowana następująco: \\
\\
zip :: [a] -> [b] -> [(a,b)] \\
zip (x:xs) (y:ys) = (x,y) : zip xs ys \\
zip \_ \_ = [] \\

Jak widać, przyjęto tutaj, że jeśli listy są różnej długości, to dłuższa lista jest ucinana.
Nie zawsze takie rozwiązanie jest satysfakcjonujące. Wymyśl taką sygnaturę dla funkcji zip na wektorach,
aby nie dopuścić (statycznie, za pomocą systemu typów) do niebezpiecznych wywołań.

\end{zadanie}

\begin{code}

vzip : {A B : Set} {n : ℕ} → Vec A n → Vec B n → Vec (A ∧ B) n
vzip [] [] = []
vzip (x ∷ xs) (x₁ ∷ ys) = and x x₁ ∷ vzip xs ys

\end{code}

\begin{zadanie}

Zaprogramuj \textbf{wydajną} funkcję odwracającą wektor. Użyj funkcji
subst z wykładu, jeśli będziesz chciał zmusić Agdę do stosowania praw
arytmetyki.

\end{zadanie}

\begin{code}

vrev' : {A : Set} {n m : ℕ} → Vec A n → Vec A m → Vec A (n + m)
vrev' [] ys = ys
vrev' {A} {suc n} {m} (x ∷ xs) ys = subst (Vec A) (n + suc m) (suc (n + m)) (symm (plus-suc-n-m n m)) (vrev' xs (x ∷ ys))

vrev : {A : Set} {n : ℕ} → Vec A n → Vec A n
vrev {A} {n} vs = subst (Vec A) (n + zero) n (plus-right-zero n) (vrev' vs [])

\end{code}

\begin{zadanie}

Rozważmy funkcję filter na wektorach. Jaka powinna być długość wektora wynikowego?
Długość ta zależy od zadanego predykatu i samego wektora ... Możliwe są trzy podejścia:
\begin{enumerate}
\item zwrócić listę zamiast wektora,
\item ukryć długość wektora używając typu egzystencjalnego ($\Sigma$ ℕ ($\lambda$ n → Vec A n)),
\item napisać pomocniczą funkcję obliczającą długość wynikowego wektora.
\end{enumerate}

Zaimplementuj wszystkie trzy warianty. W trzecim wariancie użyj następujących sygnatur:

\begin{code}
data Bool : Set where
  true false : Bool

filter-length : {A : Set}{n : ℕ} → (A → Bool) → Vec A n → ℕ
filter-length f [] = zero
filter-length f (x ∷ vs) with f x
... | true  = suc (filter-length f vs)
... | false = filter-length f vs

filter₃ : {A : Set}{n : ℕ} → (P : A → Bool) → (xs : Vec A n) → Vec A (filter-length P xs)
filter₃ f [] = []
filter₃ f (x ∷ vs) with f x 
... | true  = x ∷ filter₃ f vs
... | false = filter₃ f vs
\end{code}

\end{zadanie}

\begin{code}

data List (A : Set) : Set where
  nil : List A
  cons : A → List A → List A

filter₁ : {A : Set} {n : ℕ} → (A → Bool) → Vec A n → List A
filter₁ f [] = nil
filter₁ f (x ∷ vs) with f x 
... | true  = cons x (filter₁ f vs)
... | false = filter₁ f vs


data Σ (A : Set) (P : A → Set) : Set where
  ex : (a : A) → P a → Σ A P

filter₂ : {A : Set} {n : ℕ} → (A → Bool) → Vec A n → Σ ℕ (Vec A)
filter₂ f [] = ex 0 []
filter₂ f (x ∷ vs) with f x | filter₂ f vs
... | true  | ex n v = ex (suc n) (x ∷ v)
... | false | ex n v = ex n v

\end{code}

I bonus:

\begin{code}
nnp-em : {A : Set} → ({B : Set} → ¬ ¬ B → B) → (A ∨ ¬ A)
nnp-em f = f (λ d → d (inr (λ a → d (inl a))))

em-nnp : {A : Set} → (A ∨ ¬ A) → (¬ ¬ A → A)
em-nnp (inl x) f = x
em-nnp (inr x) f = ⊥-elim (f x)
\end{code}

Ćwiczenia rozwiązał Łukasz Dąbek.

\end{document}
